package leetCode;

import java.util.List;

/**
 * 101. Symmetric Tree
 * Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).
 * <p>
 * For example, this binary tree [1,2,2,3,4,4,3] is symmetric:
 * 1
 * / \
 * 2   2
 * / \ / \
 * 3  4 4  3
 * <p>
 * <p>
 * But the following [1,2,2,null,3,null,3] is not:
 * 1
 * / \
 * 2   2
 * \   \
 * 3    3
 * <p>
 * Note:
 * Bonus points if you could solve it both recursively and iteratively.
 */


//  Definition for a binary tree node.
class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }
}

public class SymmetricTree {

    public static void main(String[] args) {
        SymmetricTree symmetricTree = new SymmetricTree();
        System.out.println(symmetricTree.isSymmetric(null));
    }

    public boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        } else if (root.left == null && root.right == null) {
            return true;
        } else if (root.left == null || root.right == null) {
            return false;
        } else {

//            List<Integer> left = new ArrayList<>();
//            List<Integer> right = new ArrayList<>();
//            preOrderLeft(root.left, left);
//            preOrderRight(root.right, right);
//            return left.equals(right);

            return preOrder(root.left, root.right);


        }
    }

    private void preOrderLeft(TreeNode node, List<Integer> list) {
        if (node != null) {
            list.add(node.val);
            preOrderLeft(node.left, list);
            preOrderLeft(node.right, list);
        } else {
            list.add(null);
        }
    }

    private void preOrderRight(TreeNode node, List<Integer> list) {
        if (node != null) {
            list.add(node.val);
            preOrderRight(node.right, list);
            preOrderRight(node.left, list);
        } else {
            list.add(null);
        }
    }

    private boolean preOrder(TreeNode left, TreeNode right) {
        if (left != null && right != null && left.val == right.val) {
            return preOrder(left.left, right.right) && preOrder(left.right, right.left);
        } else  {
            return left == null && right == null;
        }
    }

}
