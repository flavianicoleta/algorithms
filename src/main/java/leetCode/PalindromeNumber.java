package leetCode;

/**
 * 9. Palindrome Number
 * Determine whether an integer is a palindrome. An integer is a palindrome when it reads the same backward as forward.
 * <p>
 * Example 1:
 * <p>
 * Input: 121
 * Output: true
 * Example 2:
 * <p>
 * Input: -121
 * Output: false
 * Explanation: From left to right, it reads -121. From right to left, it becomes 121-. Therefore it is not a palindrome.
 * Example 3:
 * <p>
 * Input: 10
 * Output: false
 * Explanation: Reads 01 from right to left. Therefore it is not a palindrome.
 * Follow up:
 * <p>
 * Could you solve it without converting the integer to a string?
 */
public class PalindromeNumber {

    public static void main(String[] args) {
        PalindromeNumber palindromeNumber = new PalindromeNumber();
        System.out.println(palindromeNumber.isPalindrome(1221));
        System.out.println(palindromeNumber.isPalindrome2(1001));
    }

    public boolean isPalindrome2(int x) {
        if (x < 0) {
            return false;
        } else {
            int numberOfDigits = 0;
            int copy = x;
            while (copy != 0) {
                copy = copy / 10;
                numberOfDigits++;
            }
            int divideTo = (int)Math.pow(10, numberOfDigits-1);


            while (x > 9) {
                int first = x / divideTo;
                int last = x % 10;
                if (first != last) {
                    return false;
                }
                x = (x % divideTo) / 10;
                divideTo = divideTo / 100;
            }

            if (divideTo > 1 && x != 0) {
                return false;
            }

            return true;
        }

    }

    public boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        } else {
            String str = String.valueOf(x);
            int len = str.length();
            for (int i = 0; i < len / 2; i++) {
                if (str.charAt(i) != str.charAt(len - i - 1)) {
                    return false;
                }
            }
            return true;
        }

    }
}
