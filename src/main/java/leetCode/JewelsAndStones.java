package leetCode;

import java.util.HashMap;
import java.util.Map;

/**
 * 771. Jewels and Stones
 You're given strings J representing the types of stones that are jewels, and S representing the stones you have.  Each character in S is a type of stone you have.  You want to know how many of the stones you have are also jewels.

 The letters in J are guaranteed distinct, and all characters in J and S are letters. Letters are case sensitive, so "a" is considered a different type of stone from "A".

 Example 1:

 Input: J = "aA", S = "aAAbbbb"
 Output: 3
 Example 2:

 Input: J = "z", S = "ZZ"
 Output: 0
 Note:

 S and J will consist of letters and have length at most 50.
 The characters in J are distinct.

 */
public class JewelsAndStones {

    public static void main(String[] args) {
        JewelsAndStones jewelsAndStones = new JewelsAndStones();

        String jewels = "z";
        String stones = "ZZ";

        System.out.println(jewelsAndStones.numJewelsInStones(jewels, stones));
    }

    public int numJewelsInStones(String J, String S) {

        Map<Character, Integer> jewelsMap = new HashMap<>();
        for (char c : J.toCharArray()) {
            jewelsMap.put(c, 1);
        }

        int result = 0;


        for (char c : S.toCharArray()) {
            if (jewelsMap.containsKey(c)) {
                result++;
            }
        }

        return result;

    }
}
