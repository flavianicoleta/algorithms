package coursera.algorithms_stanford_university.course_four.c_week_three;

import java.io.File;
import java.util.*;

public class GreedyTSP {

    private static int size;

    private static double[] xCoords;
    private static double[] yCoords;

    public static void main(String[] args) {
        readFile("tsp_nn.txt"); // 1203406

        long start = System.currentTimeMillis();
        tsp2();
        long stop = System.currentTimeMillis();
        System.out.println("Time running: " + (stop - start));

        System.out.println("-------------------------");

        start = System.currentTimeMillis();
        tsp1();
        stop = System.currentTimeMillis();
        System.out.println("Time running: " + (stop - start));

    }

    private static void tsp1() {
        boolean[] visited = new boolean[size];
        Map<Integer, Set<Neighbour>> cities = new HashMap<>();
        double totalCost = 0.0;

        Set<Neighbour> neighbours;

        for (int i = 0; i < size; i++) {
            neighbours = new TreeSet<>();
            for (int j = 0; j < size; j++) {
                if (i != j) {
                    double distance = Math.sqrt(Math.pow(xCoords[i] - xCoords[j], 2) + Math.pow(yCoords[i] - yCoords[j], 2));

                    neighbours.add(new Neighbour(j, distance));
                }

            }
            cities.put(i, neighbours);
        }

        Set<Integer> visitedCities = new HashSet<>();

        Set<Neighbour> currentCityNeighbours = cities.get(0);
        visited[0] = true;
        visitedCities.add(0);
        int latestVisited = 0;

        while (visitedCities.size() != cities.size()) {

            for (Neighbour neighbour : currentCityNeighbours) {
                if (!visited[neighbour.index]) {
                    visited[neighbour.index] = true;
                    currentCityNeighbours = cities.get(neighbour.index);
                    visitedCities.add(neighbour.index);
                    totalCost += neighbour.distance;
                    latestVisited = neighbour.index;
                    break;
                }
            }
        }

        totalCost += cities.get(latestVisited).stream().filter(n -> n.index == 0).findFirst().get().distance;

        System.out.println(Math.floor(totalCost));
    }

    private static void tsp2() {
        boolean[] visited = new boolean[size];
        Set<Integer> visitedCities = new HashSet<>();

        double totalCost = 0.0;
        double minCost = Double.MAX_VALUE;

        int i = 0;
        visited[i] = true;
        visitedCities.add(i);
        int nextCity = 0;
        for (int j = 0; j < size; j++) {
            double distance = Math.sqrt(Math.pow(xCoords[i] - xCoords[j], 2) + Math.pow(yCoords[i] - yCoords[j], 2));
            if (distance < minCost && i != j) {
                minCost = distance;
                nextCity = j;
            }
        }


        visited[nextCity] = true;
        visitedCities.add(nextCity);
        totalCost += minCost;

        while (visitedCities.size() < size) {
            minCost = Double.MAX_VALUE;
            i = nextCity;
            visited[i] = true;
            for (int j = 0; j < size; j++) {
                double distance = Math.sqrt(Math.pow(xCoords[i] - xCoords[j], 2) + Math.pow(yCoords[i] - yCoords[j], 2));
                if (distance < minCost && !visited[j] && i != j) {
                    minCost = distance;
                    nextCity = j;
                }
            }
            visited[nextCity] = true;
            visitedCities.add(nextCity);
            totalCost += minCost;
        }

        double distance = Math.sqrt(Math.pow(xCoords[nextCity] - xCoords[0], 2) + Math.pow(yCoords[nextCity] - yCoords[0], 2));
        totalCost += distance;

        System.out.println(Math.floor(totalCost));
    }


    private static void readFile(String fileName) {
        ClassLoader classLoader = new GreedyTSP().getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try {
            Scanner sc = new Scanner(file);

            size = sc.nextInt();

            xCoords = new double[size];
            yCoords = new double[size];
            int i = 0;

            while (sc.hasNext()) {
                sc.nextInt();
                xCoords[i] = sc.nextDouble();
                yCoords[i] = sc.nextDouble();
                i++;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static class Neighbour implements Comparable<Neighbour> {
        int index;

        double distance;

        Neighbour(int index, double distance) {
            this.index = index;
            this.distance = distance;
        }

        @Override
        public int compareTo(Neighbour o) {
            if (distance < o.distance) {
                return -1;
            } else if (distance > o.distance) {
                return 1;
            } else {
                if (index < o.index) {
                    return -1;
                } else if (index > o.index) {
                    return 1;
                }
            }
            return 0;
        }

        @Override
        public String toString() {
            return "Neighbour{" +
                    "index=" + index +
                    ", distance=" + distance +
                    '}';
        }
    }
}
