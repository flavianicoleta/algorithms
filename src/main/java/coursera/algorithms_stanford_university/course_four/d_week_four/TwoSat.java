package coursera.algorithms_stanford_university.course_four.d_week_four;

import java.io.File;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;

/**
 * Papadimitriou's 2SAT Algorithm
 */

public class TwoSat {

    private static int size;

    private static int[] firstClauses;

    private static int[] secondClauses;

    private static boolean[] assignments;

    private static Random random = new Random();

    private static int[] negatedVariables;
    private static int[] nonNegatedVariables;

    public static void main(String[] args) {

        // 101100
        readFile("2_SAT_1.txt");
        run();

        readFile("2_SAT_2.txt");
        run();

        readFile("2_SAT_3.txt");
        run();

        readFile("2_SAT_4.txt");
        run();

        readFile("2_SAT_5.txt");
        run();

        readFile("2_SAT_6.txt");
        run();
    }

    private static void reduce() {
        Set<Integer> clausesToReduce = new HashSet<>();

        for (int i = 0; i < size; i++) {

            int first = firstClauses[i];
            int second = secondClauses[i];

            if (negatedVariables[Math.abs(first)] == 0 || nonNegatedVariables[Math.abs(first)] == 0) {
                clausesToReduce.add(i);
                if (second < 0 && negatedVariables[Math.abs(second)] > 0) {
                    negatedVariables[Math.abs(second)]--;
                } else if (nonNegatedVariables[second] > 0){
                    nonNegatedVariables[second]--;
                }
            }

            if (negatedVariables[Math.abs(second)] == 0 || nonNegatedVariables[Math.abs(second)] == 0) {
                clausesToReduce.add(i);
                if (first < 0 && negatedVariables[Math.abs(first)] > 0) {
                    negatedVariables[Math.abs(first)]--;
                } else if (nonNegatedVariables[first] > 0) {
                    nonNegatedVariables[first]--;
                }
            }
        }

        if (clausesToReduce.size() > 0) {
            int[] toReduceArr = clausesToReduce.stream().mapToInt(i->i).toArray();
            firstClauses = ArrayUtils.removeAll(firstClauses, toReduceArr);
            secondClauses = ArrayUtils.removeAll(secondClauses, toReduceArr);
            size = size - toReduceArr.length;
            reduce();
        }
    }

    private static void run() {
        reduce();
        int totalRuns = (int) (Math.log(size) / Math.log(2));

        for (int i = 0; i < totalRuns; i++) {
            setInitialRandomAssignment();

            boolean res = runAssignment();
            if (res) {
                System.out.println("satisfiable");
                return;
            }
        }
        System.out.println("NOT satisfiable");

    }

    private static boolean runAssignment() {
        long maxRuns = Math.multiplyExact(2, (long) Math.pow(size, 2));

        int count = 0;
        while (count < maxRuns) {

            int res = checkAssigment();
            if (res == -1) {
                return true;
            } else {
                boolean clauseToChange = random.nextBoolean();
                if (clauseToChange) {
                    assignments[Math.abs(firstClauses[res])] = !assignments[Math.abs(firstClauses[res])];
                } else {
                    assignments[Math.abs(secondClauses[res])] = !assignments[Math.abs(secondClauses[res])];
                }
            }

            count++;
        }
        return false;
    }

    private static void setInitialRandomAssignment() {
        for (int i = 1; i <= size; i++) {
            assignments[i] = random.nextBoolean();
        }
    }

    private static int checkAssigment() {
        for (int i = 0; i < size; i++) {
            boolean first;
            if (firstClauses[i] < 0) {
                first = !assignments[Math.abs(firstClauses[i])];
            } else {
                first = assignments[firstClauses[i]];
            }

            boolean second;
            if (secondClauses[i] < 0) {
                second = !assignments[Math.abs(secondClauses[i])];
            } else {
                second = assignments[secondClauses[i]];
            }

            if (!(first || second)) {
                return i;
            }
        }

        return -1;
    }

    private static void readFile(String fileName) {
        ClassLoader classLoader = new TwoSat().getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try {
            Scanner sc = new Scanner(file);

            size = sc.nextInt();
            firstClauses = new int[size];
            secondClauses = new int[size];
            assignments = new boolean[size + 1];

            int i = 0;

            negatedVariables = new int[size + 1];
            nonNegatedVariables = new int[size + 1];

            while (sc.hasNext()) {
                int first = sc.nextInt();
                firstClauses[i] = first;

                if (first < 0) {
                    negatedVariables[Math.abs(first)]++;
                } else {
                    nonNegatedVariables[first]++;
                }

                int second = sc.nextInt();
                secondClauses[i] = second;
                if (second < 0) {
                    negatedVariables[Math.abs(second)]++;
                } else {
                    nonNegatedVariables[second]++;
                }

                i++;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
