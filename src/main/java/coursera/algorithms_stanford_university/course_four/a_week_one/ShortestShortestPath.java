package coursera.algorithms_stanford_university.course_four.a_week_one;

import java.io.File;
import java.util.Scanner;

public class ShortestShortestPath {

    private static int size;

    private static int[][] edges;

    private static int[][][] result;

    private static final int MAX = 999999;

    public static void main(String[] args) {

        // g1 - NULL
        // g2 - NULL
        // g3 - -19

        readFile("g3.txt");

        floydWarshallAlg();

//        printMatrix();

        searchForMin();
    }

    private static void floydWarshallAlg() {

        result = new int[size][size][2];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (i == j) {
                    result[i][j][0] = 0;
                } else {
                    result[i][j][0] = edges[i][j];
                }
            }
        }

        for (int k = 0; k < size; k++) {
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    result[i][j][1] = Math.min(result[i][j][0], result[i][k][0] + result[k][j][0]);
                }
            }

            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    result[i][j][0] = result[i][j][1];
                }
            }
        }

    }

    private static void printMatrix() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {

                if (result[i][j][0] == MAX) {
                    System.out.print("MAX ");
                } else {
                    System.out.print(result[i][j][0] + " ");
                }
            }
            System.out.println();
        }

        System.out.println();

    }

    private static void searchForMin() {
        for (int i = 0; i < size; i++) {
            if (result[i][i][1] < 0) {
                System.out.println("NULL");
                return;
            }
        }

        int min = MAX;

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (result[i][j][1] < min) {
                    min = result[i][j][1];
                }
            }
        }

        System.out.println("MIN: " + min);
    }


    private static void readFile(String fileName) {
        ClassLoader classLoader = new ShortestShortestPath().getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try {
            Scanner sc = new Scanner(file);

            size = sc.nextInt();
            sc.nextInt();

            edges = new int[size][size];

            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    if (i == j) {
                        edges[i][j] = 0;
                    } else {
                        edges[i][j] = MAX;
                    }
                }
            }

            while (sc.hasNext()) {
                int firstNode = sc.nextInt();
                int secondNode = sc.nextInt();
                int cost = sc.nextInt();

                edges[firstNode - 1][secondNode - 1] = cost;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
