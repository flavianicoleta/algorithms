package coursera.algorithms_stanford_university.course_four.b_week_two;

import java.io.File;
import java.util.*;

public class TravelingSalesmanProblem {

    private static int size;

    private static Map<int[], Integer> indexes = new HashMap<>();

    private static int currentIndex = 0;

    private static double[][] distances;

    private static double[][] result;

    private static final double MAX = Double.MAX_VALUE;

    public static void main(String[] args) {
        readFile("tsp.txt");

    }

    private static void tsp() {

        Map<Integer, List<int[]>> allSubsets = new HashMap<>();

        int totalSize = 0;

        for (int m = 1; m < size; m++) {
            List<int[]> subsets = subsets(m);
            allSubsets.put(m, subsets);
            totalSize += subsets.size();
        }

        System.out.println();

        result = new double[totalSize][size];
        result[0][0] = 0;
        for (int s = 1; s < size; s++) {
            result[s][0] = MAX;
        }

        for (int m = 1; m < size; m++) {
            List<int[]> subsets = allSubsets.get(m);
            List<int[]> previousSubsets = allSubsets.get(m - 1);
            for (int[] subset : subsets) {
                for (int j : subset) {

                    if (j != 0) {



//                        result[indexes.get(subset)][j] =
                    }
                }
            }
        }
    }

    private static List<int[]> subsets(int m) {
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = i;
        }

        int[] data = new int[m];
        List<int[]> res = new ArrayList<>();

        subsets(arr, m, 0, data, 0, 0, res);

        return res;
    }

    private static void subsets(int[] arr, int m,
                                int index, int[] data, int i, int j, List<int[]> res) {
        if (index == m) {
            int[] copyOfData = Arrays.copyOf(data, m);
            res.add(copyOfData);
            indexes.put(copyOfData, currentIndex++);
            return;
        }

        if (i >= size || data[0] != j) {
            return;
        }


        data[index] = arr[i];
        subsets(arr, m, index + 1, data, i + 1, 0, res);
        subsets(arr, m, index, data, i + 1, 0, res);
    }

    private static void readFile(String fileName) {
        ClassLoader classLoader = new TravelingSalesmanProblem().getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try {
            Scanner sc = new Scanner(file);

            size = sc.nextInt();
            double[] xCoords = new double[size];
            double[] yCoords = new double[size];
            int i = 0;

            while (sc.hasNext()) {
                xCoords[i] = sc.nextDouble();
                yCoords[i] = sc.nextDouble();
                i++;
            }

            distances = new double[size][size];


            for (i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    distances[i][j] = Math.sqrt(Math.pow(xCoords[i] - xCoords[j], 2) + Math.pow(yCoords[i] - yCoords[j], 2));
                }
            }


        } catch (
                Exception e) {
            e.printStackTrace();
        }
    }
}
