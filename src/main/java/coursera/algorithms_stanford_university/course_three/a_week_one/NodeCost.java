package coursera.algorithms_stanford_university.course_three.a_week_one;

public class NodeCost {

    private int node;

    private int cost;

    public NodeCost(int node, int cost) {
        this.node = node;
        this.cost = cost;
    }

    public int getNode() {
        return node;
    }

    public void setNode(int node) {
        this.node = node;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "NodeCost{" +
                "node=" + node +
                ", cost=" + cost +
                '}';
    }
}
