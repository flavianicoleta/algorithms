package coursera.algorithms_stanford_university.course_three.a_week_one;

public class Job {

    private int weight;

    private int length;

    private int difference;

    private double ratio;

    public Job(int weight, int length) {
        this.weight = weight;
        this.length = length;

        this.difference = this.weight - this.length;

        this.ratio = (double) this.weight/ (double) this.length;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getDifference() {
        return difference;
    }

    public void setDifference(int difference) {
        this.difference = difference;
    }

    public double getRatio() {
        return ratio;
    }

    public void setRatio(double ratio) {
        this.ratio = ratio;
    }

    @Override
    public String toString() {
        return "Job{" +
                "weight=" + weight +
                ", length=" + length +
                ", difference=" + difference +
                ", ratio=" + ratio +
                '}';
    }
}
