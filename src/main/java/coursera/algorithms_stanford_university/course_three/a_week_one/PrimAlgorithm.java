package coursera.algorithms_stanford_university.course_three.a_week_one;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

public class PrimAlgorithm {

    private static int nodesSize;

    private static Map<Integer, List<NodeCost>> graph = new HashMap<>();


    public static void main(String[] args) {
        readFile("prim_edges.txt");

        prim();
    }

    private static void prim() {
        int overallCost = 0; //-3612829
        Random random = new Random();
        int chosenNode = random.nextInt(nodesSize);

        Set<Integer> spannedNodes = new HashSet<>();
        spannedNodes.add(chosenNode);

        List<NodeCost> nodeCosts = graph.get(chosenNode);

        NodeCost cheapestNode = nodeCosts.get(0);

        spannedNodes.add(cheapestNode.getNode());
        overallCost += cheapestNode.getCost();


        while (spannedNodes.size() != nodesSize) {

            int minCost = Integer.MAX_VALUE;
            int nodeWithMinCost = 0;

            for (int spannedNode : spannedNodes) {
                nodeCosts = graph.get(spannedNode).stream().filter(cn -> !spannedNodes.contains(cn.getNode())).collect(Collectors.toList());

                if (nodeCosts.size() > 0 && nodeCosts.get(0).getCost() < minCost) {
                    minCost = nodeCosts.get(0).getCost();
                    nodeWithMinCost = nodeCosts.get(0).getNode();
                }

            }
            spannedNodes.add(nodeWithMinCost);
            overallCost += minCost;

        }

        System.out.println(overallCost);
    }

    private static void readFile(String fileName) {
        ClassLoader classLoader = new PrimAlgorithm().getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try {
            Scanner sc = new Scanner(file);

            nodesSize = sc.nextInt();
            sc.nextInt();

            while (sc.hasNext()) {

                int firstNode = sc.nextInt();
                int secondNode = sc.nextInt();
                int cost = sc.nextInt();

                NodeCost nodeCost1 = new NodeCost(firstNode, cost);
                NodeCost nodeCost2 = new NodeCost(secondNode, cost);

                if (graph.containsKey(firstNode)) {
                    graph.get(firstNode).add(nodeCost2);
                } else {
                    List<NodeCost> costList = new ArrayList<>();
                    costList.add(nodeCost2);
                    graph.put(firstNode, costList);
                }

                if (graph.containsKey(secondNode)) {
                    graph.get(secondNode).add(nodeCost1);
                } else {
                    List<NodeCost> costList = new ArrayList<>();
                    costList.add(nodeCost1);
                    graph.put(secondNode, costList);
                }

            }

            for(Map.Entry<Integer, List<NodeCost>> entry : graph.entrySet()) {
                entry.getValue().sort(new MinComparator());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static class MinComparator implements Comparator<NodeCost> {

        @Override
        public int compare(NodeCost o1, NodeCost o2) {
            if (o1.getCost() < o2.getCost()) {
                return -1;
            } else if (o1.getCost() > o2.getCost()) {
                return 1;
            }
            return 0;
        }
    }
}