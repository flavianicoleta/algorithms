package coursera.algorithms_stanford_university.course_three.a_week_one;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class SchedulingGreedyAlg {

    private static List<Job> jobs = new ArrayList<>();
    private static int length;

    public static void main(String args[]) {
        readFile("jobs.txt");

        jobs.sort(new DifferenceComparator());

        greedyAlgorithm();

        jobs.sort(new RatioComparator());

        greedyAlgorithm();

    }

    private static void greedyAlgorithm() {
        long completionTime = 0;

        long weightedCompletion = 0; //69119377652; 67311454237

        for (Job job : jobs) {
            completionTime += job.getLength();

            weightedCompletion = weightedCompletion + (job.getWeight() * completionTime);
        }

        System.out.println(weightedCompletion);
    }

    private static void readFile(String fileName) {
        ClassLoader classLoader = new SchedulingGreedyAlg().getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try {
            Scanner sc = new Scanner(file);

            length = sc.nextInt();

            while (sc.hasNext()) {
                Job job = new Job(sc.nextInt(), sc.nextInt());

                jobs.add(job);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static class DifferenceComparator implements Comparator<Job> {

        @Override
        public int compare(Job o1, Job o2) {


            if (o1.getDifference() < o2.getDifference()) {
                return 1;
            } else if (o1.getDifference() > o2.getDifference()) {
                return -1;
            } else {
                if (o1.getWeight() < o2.getWeight()) {
                    return 1;
                } else if (o1.getWeight() > o2.getWeight()) {
                    return -1;
                }
            }
            return 0;
        }
    }

    private static class RatioComparator implements Comparator<Job> {

        @Override
        public int compare(Job o1, Job o2) {
            if (o1.getRatio() > o2.getRatio()) {
                return -1;
            } else if (o1.getRatio() < o2.getRatio()) {
                return 1;
            }
            return 0;
        }
    }
}
