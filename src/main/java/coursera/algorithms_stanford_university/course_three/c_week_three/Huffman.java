package coursera.algorithms_stanford_university.course_three.c_week_three;

import java.io.File;
import java.util.*;

public class Huffman {

    private static int totalSize;

    private static List<HuffmanNode> huffmanNodes = new ArrayList<>();

    public static void main(String[] args) {

        readFile("huffman.txt");
//        readFile("huffman_small.txt");

        huffmanNodes.sort(new MinComparator());

        huffmanAlg();

        System.out.println(findMax(huffmanNodes.get(0)) - 1); // 19
        System.out.println(findMin(huffmanNodes.get(0)) - 1); // 9
    }

    private static int findMin(HuffmanNode node) {
        if (node == null) return 0;
        if (node.getRight() == null && node.getLeft() == null) return 1;

        if (node.getLeft() == null) {
            return findMin(node.getRight()) + 1;
        }

        if (node.getRight() == null) {
            return findMin(node.getLeft()) + 1;
        }

        return Math.min(findMin(node.getLeft()), findMin(node.getRight())) + 1;
    }

    private static int findMax(HuffmanNode node) {
        if (node == null) return 0;
        else {
            int left = findMax(node.getLeft());
            int right = findMax(node.getRight());

            return Math.max(left, right) + 1;
        }
    }


    private static void huffmanAlg() {
        while (huffmanNodes.size() > 1) {
            HuffmanNode firstNode = huffmanNodes.get(0);
            HuffmanNode secondNode = huffmanNodes.get(1);

            HuffmanNode parentNode = new HuffmanNode(firstNode.getSymbol() + "_" + secondNode.getSymbol(), firstNode.getWeight() + secondNode.getWeight(), firstNode.getPathCount() + secondNode.getPathCount());

            parentNode.setLeft(firstNode);
            parentNode.setRight(secondNode);

            if (huffmanNodes.get(0).getWeight() > parentNode.getWeight()) {
                huffmanNodes.add(0, parentNode);
            } else if (huffmanNodes.get(huffmanNodes.size() - 1).getWeight() < parentNode.getWeight()) {
                huffmanNodes.add(huffmanNodes.size(), parentNode);
            } else {
                for (int i = 0; i < huffmanNodes.size(); i++) {
                    if (huffmanNodes.get(i).getWeight() > parentNode.getWeight()) {
                        huffmanNodes.add(i, parentNode);
                        break;
                    }
                }
            }

            huffmanNodes.remove(firstNode);
            huffmanNodes.remove(secondNode);


        }
    }

    private static void readFile(String fileName) {
        ClassLoader classLoader = new Huffman().getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try {
            Scanner sc = new Scanner(file);
            totalSize = sc.nextInt();

            int nr = 1;
            while (sc.hasNextInt()) {
                HuffmanNode huffmanNode = new HuffmanNode(String.valueOf(nr), sc.nextInt(), 1);

                huffmanNodes.add(huffmanNode);
                nr++;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static class MinComparator implements Comparator<HuffmanNode> {

        @Override
        public int compare(HuffmanNode o1, HuffmanNode o2) {
            if (o1.getWeight() < o2.getWeight()) {
                return -1;
            } else if (o1.getWeight() > o2.getWeight()) {
                return 1;
            }
            return 0;
        }
    }
}
