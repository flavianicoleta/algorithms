package coursera.algorithms_stanford_university.course_three.c_week_three;

import java.io.File;
import java.util.Arrays;
import java.util.Scanner;

public class MaximumWeightIndependentSet {

    private static int[] vertices;

    private static int[] solutionWeights;
    
    private static byte[] optimalSolution;

    public static void main(String[] args) {

//        readFile("small_mwis.txt");
        readFile("mwis.txt");

        System.out.println(Arrays.toString(vertices));

        maximumWeightAlg();

        System.out.println(Arrays.toString(solutionWeights));

        reconstructionAlg();

        System.out.println(Arrays.toString(optimalSolution));

        String result = "" + optimalSolution[1] + optimalSolution[2] + optimalSolution[3] + optimalSolution[4];
        if (optimalSolution.length > 17) {
            result += optimalSolution[17];
        } else {
            result += "0";
        }

        if (optimalSolution.length > 117) {
            result += optimalSolution[117];
        } else {
            result += "0";
        }

        if (optimalSolution.length > 517) {
            result += optimalSolution[517];
        } else {
            result += "0";
        }

        if (optimalSolution.length > 997) {
            result += optimalSolution[997];
        } else {
            result += "0";
        }

        System.out.println(result);
    }

    private static void maximumWeightAlg() {
        solutionWeights = new int[vertices.length];

        solutionWeights[0] = 0;
        solutionWeights[1] = vertices[1];

        for (int i = 2; i < vertices.length; i++) {
            solutionWeights[i] = Math.max(solutionWeights[i - 1], solutionWeights[i - 2] + vertices[i]);
        }
    }

    private static void reconstructionAlg() {
        optimalSolution = new byte[solutionWeights.length];

        int i = solutionWeights.length;
        while (i > 1) {
            if (solutionWeights[i - 1] >= solutionWeights[i - 2] + vertices[i - 1]) {
                i--;
            } else {
                i = i -2;
                optimalSolution[i] = 1;
            }
        }
    }

    private static void readFile(String fileName) {
        ClassLoader classLoader = new MaximumWeightIndependentSet().getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try {
            Scanner sc = new Scanner(file);

            vertices = new int[sc.nextInt() + 1];
            int i = 1;
            while (sc.hasNextInt()) {
                vertices[i] = sc.nextInt();
                i++;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
