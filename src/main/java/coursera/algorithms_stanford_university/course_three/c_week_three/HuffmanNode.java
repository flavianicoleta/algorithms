package coursera.algorithms_stanford_university.course_three.c_week_three;

public class HuffmanNode {

    private String symbol;

    private int weight;

    private HuffmanNode left;

    private HuffmanNode right;

    private int pathCount;

    private boolean explored;

    public HuffmanNode(String symbol, int weight, int pathCount) {
        this.symbol = symbol;
        this.weight = weight;
        this.pathCount = pathCount;
    }

    public String getSymbol() {
        return symbol;
    }

    public int getPathCount() {
        return pathCount;
    }

    public void setPathCount(int pathCount) {
        this.pathCount = pathCount;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public HuffmanNode getLeft() {
        return left;
    }

    public void setLeft(HuffmanNode left) {
        this.left = left;
    }

    public HuffmanNode getRight() {
        return right;
    }

    public void setRight(HuffmanNode right) {
        this.right = right;
    }

    public boolean isExplored() {
        return explored;
    }

    public void setExplored(boolean explored) {
        this.explored = explored;
    }

    @Override
    public String toString() {
        return "HuffmanNode{" +
                "symbol='" + symbol + '\'' +
                ", weight=" + weight +
                ", left=" + left +
                ", right=" + right +
                '}';
    }
}
