package coursera.algorithms_stanford_university.course_three.d_week_four;

import java.io.File;
import java.util.Arrays;
import java.util.Scanner;

public class KnapsackProblem {


    private static int size;

    private static int maxWeight;

    private static int[] weights;

    private static int[] values;

    private static int[][] result;

    private static int[] computedWeights;

    public static void main(String[] args) {
//        readFile("knapsack_small.txt");

        // 2493893
        readFile("knapsack1.txt");
        knapsackAlgorithm();
        System.out.println("Problem 1: " + result[size][maxWeight]);

        // 4243395
        readFile("knapsack_big.txt");
        knapsackAlgorithm2();
        System.out.println("Problem 2: " + computedWeights[maxWeight]);

    }

    private static void knapsackAlgorithm() {
        result = new int[size + 1][maxWeight + 1];

        for (int i = 0; i <= maxWeight; i++) {
            result[0][i] = 0;
        }

        for (int i = 1; i <= size; i++) {
            for (int x = 0; x <= maxWeight; x++) {
                if (weights[i] <= x) {
                    result[i][x] = Math.max(result[i - 1][x], result[i - 1][x - weights[i]] + values[i]);
                } else {
                    result[i][x] = result[i - 1][x];
                }
            }
        }
    }

    private static void knapsackAlgorithm2() {
        int[] currentWeights = new int[maxWeight + 1];
        computedWeights = new int[maxWeight + 1];

        for (int i = 0; i <= maxWeight; i++) {
            computedWeights[i] = 0;
        }

        int i = 1;
        while (i <= size) {

            for (int x = 0; x <= maxWeight; x++) {
                if (weights[i] <= x) {
                    currentWeights[x] = Math.max(computedWeights[x], computedWeights[x - weights[i]] + values[i]);
                } else {
                    currentWeights[x] = computedWeights[x];
                }
            }

            computedWeights = Arrays.copyOf(currentWeights, currentWeights.length);

            i++;
        }

    }

    private static void readFile(String fileName) {
        ClassLoader classLoader = new KnapsackProblem().getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try {
            Scanner sc = new Scanner(file);

            maxWeight = sc.nextInt();

            size = sc.nextInt();

            weights = new int[size + 1];
            values = new int[size + 1];

            int i = 1;
            while (sc.hasNextInt()) {
                values[i] = sc.nextInt();
                weights[i] = sc.nextInt();
                i++;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
