package coursera.algorithms_stanford_university.course_three.b_week_two;

public class Node {

    private int value;

    private int bitsValue;

    private boolean explored;

    public Node(int value, int bitsValue) {
        this.value = value;
        this.bitsValue = bitsValue;
        this.explored = false;
    }

    public Node(int value) {
        this.value = value;
        this.explored = false;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isExplored() {
        return explored;
    }

    public void setExplored(boolean explored) {
        this.explored = explored;
    }

    public int getBitsValue() {
        return bitsValue;
    }

    public void setBitsValue(int bitsValue) {
        this.bitsValue = bitsValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Node node = (Node) o;

        return value == node.value;
    }

    @Override
    public int hashCode() {
        return value;
    }

    @Override
    public String toString() {
        return "Node{" +
                "value=" + value +
                '}';
    }
}
