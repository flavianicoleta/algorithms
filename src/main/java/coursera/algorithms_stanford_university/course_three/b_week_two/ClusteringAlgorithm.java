package coursera.algorithms_stanford_university.course_three.b_week_two;

import java.io.File;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;

public class ClusteringAlgorithm {

    private static final int CLUSTERS = 4;

    private static int currentNumberOfClusters;

    private static List<Edge> edges;

    private static Set<Node> minimalSpanningTree = new HashSet<>();

    private static Map<Node, List<Node>> mstNodesMap = new HashMap<>();

    public static void main(String[] args) {
        readFile("clustering1.txt");
//        readFile("clustering_1small.txt");

        edges.sort(new MinComparator());

        kruskalAlg();

    }
     // 106 - simple version
    private static void kruskalAlg() {
        for (Edge edge : edges) {
            if (currentNumberOfClusters == CLUSTERS) {

                boolean isMax = bfs(edge.getFirstNode(), edge.getSecondNode());
                if (!isMax ){
                    System.out.println(edge.getCost());

                    break;
                } else {
                    resetNodesToUnexplored();
                    continue;
                }

            }
            if (minimalSpanningTree.contains(edge.getFirstNode()) && minimalSpanningTree.contains(edge.getSecondNode())) {
                if (!bfs(edge.getFirstNode(), edge.getSecondNode())) {
                    minimalSpanningTree.add(edge.getFirstNode());
                    minimalSpanningTree.add(edge.getSecondNode());

                    if (mstNodesMap.containsKey(edge.getFirstNode())) {
                        List<Node> n1 = mstNodesMap.get(edge.getFirstNode());
                        n1.add(edge.getSecondNode());
                        mstNodesMap.put(edge.getFirstNode(), n1);
                    } else {
                        List<Node> n1 = new ArrayList<>();
                        n1.add(edge.getSecondNode());
                        mstNodesMap.put(edge.getFirstNode(), n1);
                    }

                    if (mstNodesMap.containsKey(edge.getSecondNode())) {
                        List<Node> n2 = mstNodesMap.get(edge.getSecondNode());
                        n2.add(edge.getFirstNode());
                        mstNodesMap.put(edge.getSecondNode(), n2);
                    } else {
                        List<Node> n2 = new ArrayList<>();
                        n2.add(edge.getFirstNode());
                        mstNodesMap.put(edge.getSecondNode(), n2);
                    }

                    currentNumberOfClusters--;
                    resetNodesToUnexplored();
                } else {
                    resetNodesToUnexplored();
                }
            } else {
                minimalSpanningTree.add(edge.getFirstNode());
                minimalSpanningTree.add(edge.getSecondNode());

                if (mstNodesMap.containsKey(edge.getFirstNode())) {
                    List<Node> n1 = mstNodesMap.get(edge.getFirstNode());
                    n1.add(edge.getSecondNode());
                    mstNodesMap.put(edge.getFirstNode(), n1);
                } else {
                    List<Node> n1 = new ArrayList<>();
                    n1.add(edge.getSecondNode());
                    mstNodesMap.put(edge.getFirstNode(), n1);
                }

                if (mstNodesMap.containsKey(edge.getSecondNode())) {
                    List<Node> n2 = mstNodesMap.get(edge.getSecondNode());
                    n2.add(edge.getFirstNode());
                    mstNodesMap.put(edge.getSecondNode(), n2);
                } else {
                    List<Node> n2 = new ArrayList<>();
                    n2.add(edge.getFirstNode());
                    mstNodesMap.put(edge.getSecondNode(), n2);
                }

                currentNumberOfClusters--;

            }
        }
    }

    private static boolean bfs(Node first, Node second) {

        Queue<Node> queue = new ArrayDeque<>();
        first.setExplored(true);
        queue.add(first);

        while (!queue.isEmpty()) {

            Node initial = queue.poll();
            List<Node> nodeList = mstNodesMap.getOrDefault(initial, null);

            if (nodeList != null) {
                for (Node n : nodeList) {
                    if (n.getValue() == second.getValue()) {
                        return true;
                    } else {
                        if (!n.isExplored()) {
                            n.setExplored(true);
                            queue.add(n);
                        }
                    }
                }
            }
        }

        return false;
    }

    private static void resetNodesToUnexplored() {
        for (Map.Entry<Node, List<Node>> entry : mstNodesMap.entrySet()) {
            entry.getKey().setExplored(false);
            entry.getValue().forEach(n -> n.setExplored(false));
        }

        edges.forEach(n -> {n.getFirstNode().setExplored(false); n.getSecondNode().setExplored(false);});
    }

    private static void readFile(String fileName) {
        ClassLoader classLoader = new ClusteringAlgorithm().getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try {
            Scanner sc = new Scanner(file);

            currentNumberOfClusters = sc.nextInt();

            edges = new ArrayList<>();

            while (sc.hasNextInt()) {
                Node firstNode = new Node(sc.nextInt());

                Node secondNode = new Node(sc.nextInt());

                int cost = sc.nextInt();

                Edge edge = new Edge(firstNode, secondNode, cost);

                edges.add(edge);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static class MinComparator implements Comparator<Edge> {

        @Override
        public int compare(Edge o1, Edge o2) {
            if (o1.getCost() < o2.getCost()) {
                return -1;
            } else if (o1.getCost() > o2.getCost()) {
                return 1;
            }
            return 0;
        }
    }
}
