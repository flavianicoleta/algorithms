package coursera.algorithms_stanford_university.course_three.b_week_two;

import java.io.File;
import java.util.*;

public class HammingClusteringAlg {

//    private static L<Integer> powersOf2 = new HashSet<>();

//    private static int[] powersOf2 = new int[24];
//    private static int[] sumOfPowersOf2 = new int[276];

    private static final char XOR_CONSTANT = '1';

    private static Map<String, List<String>> zeroHammingDistances = new HashMap<>();

    private static Map<String, List<String>> oneHammingDistances = new HashMap<>();

    private static Map<String, List<String>> twoHammingDistances = new HashMap<>();

    private static Set<Edge> edgesWithCostOne = new HashSet<>();

    private static Set<Edge> edgesWithCostTwo = new HashSet<>();

    private static Map<Integer, Integer> nodesWithValues = new HashMap<>();
    private static Map<String, List<Integer>> verticesWithCodeAndIndex = new HashMap<>();

    private static int BITS;

    private static int currentNumberOfClusters;

    private static Set<Node> minimalSpanningTree = new HashSet<>();

    private static Map<Node, List<Node>> mstNodesMap = new HashMap<>();

    private static Set<Edge> edges = new HashSet<>();

    public static void main(String[] args) {

        readFile("clustering_big.txt");
//        readFile("clustering_2small.txt");

//        System.out.println(verticesWithCodeAndIndex);

        for (Map.Entry<String, List<Integer>> entry : verticesWithCodeAndIndex.entrySet()) {

            for (int firstVertex : entry.getValue()) {

//                for (String possVertex : zeroHammingDistances.get(entry.getKey())) {
//                    if (verticesWithCodeAndIndex.containsKey(possVertex)) {
//
//                        for (int secondVertex : verticesWithCodeAndIndex.get(possVertex)) {
//
//                            Node n1 = new Node(firstVertex);
//                            Node n2 = new Node(secondVertex);
//
//                            Edge edge = new Edge(n1, n2, 0);
//
//                            edges.add(edge);
//                        }
//                    }
//                }

                for (String possVertex : oneHammingDistances.get(entry.getKey())) {
                    if (verticesWithCodeAndIndex.containsKey(possVertex)) {

                        for (int secondVertex : verticesWithCodeAndIndex.get(possVertex)) {

                            Node n1 = new Node(firstVertex);
                            Node n2 = new Node(secondVertex);

                            Edge edge = new Edge(n1, n2, 1);

                            edges.add(edge);
                        }
                    }
                }

                for (String possVertex : twoHammingDistances.get(entry.getKey())) {
                    if (verticesWithCodeAndIndex.containsKey(possVertex)) {

                        for (int secondVertex : verticesWithCodeAndIndex.get(possVertex)) {

                            Node n1 = new Node(firstVertex);
                            Node n2 = new Node(secondVertex);

                            Edge edge = new Edge(n1, n2, 2);

                            edges.add(edge);
                        }
                    }
                }

            }


        }

//        System.out.println(currentNumberOfClusters);
//
        kruskalAlg();
//
        System.out.println(currentNumberOfClusters);
//
//        kruskalAlg(edgesWithCostTwo);
//
//        System.out.println(currentNumberOfClusters);

    }

    private static int hammingDistance(String first, String second) {
        int count = 0;
        for (int i = 0; i < first.length(); i++) {
            if (first.charAt(i) != second.charAt(i)) {
                count++;
            }
        }
        return count;
    }

    private static void kruskalAlg() {
        for (Edge edge : edges) {
//            if (currentNumberOfClusters == CLUSTERS) {
//
//                boolean isMax = bfs(edge.getFirstNode(), edge.getSecondNode());
//                if (!isMax ){
//                    System.out.println(edge.getCost());
//
//                    break;
//                } else {
//                    resetNodesToUnexplored();
//                    continue;
//                }
//
//            }
            if (minimalSpanningTree.contains(edge.getFirstNode()) && minimalSpanningTree.contains(edge.getSecondNode())) {
                if (!bfs(edge.getFirstNode(), edge.getSecondNode())) {
                    minimalSpanningTree.add(edge.getFirstNode());
                    minimalSpanningTree.add(edge.getSecondNode());

                    if (mstNodesMap.containsKey(edge.getFirstNode())) {
                        List<Node> n1 = mstNodesMap.get(edge.getFirstNode());
                        n1.add(edge.getSecondNode());
                        mstNodesMap.put(edge.getFirstNode(), n1);
                    } else {
                        List<Node> n1 = new ArrayList<>();
                        n1.add(edge.getSecondNode());
                        mstNodesMap.put(edge.getFirstNode(), n1);
                    }

                    if (mstNodesMap.containsKey(edge.getSecondNode())) {
                        List<Node> n2 = mstNodesMap.get(edge.getSecondNode());
                        n2.add(edge.getFirstNode());
                        mstNodesMap.put(edge.getSecondNode(), n2);
                    } else {
                        List<Node> n2 = new ArrayList<>();
                        n2.add(edge.getFirstNode());
                        mstNodesMap.put(edge.getSecondNode(), n2);
                    }

                    currentNumberOfClusters--;
                    resetNodesToUnexplored();
                } else {
                    resetNodesToUnexplored();
                }
            } else {
                minimalSpanningTree.add(edge.getFirstNode());
                minimalSpanningTree.add(edge.getSecondNode());

                if (mstNodesMap.containsKey(edge.getFirstNode())) {
                    List<Node> n1 = mstNodesMap.get(edge.getFirstNode());
                    n1.add(edge.getSecondNode());
                    mstNodesMap.put(edge.getFirstNode(), n1);
                } else {
                    List<Node> n1 = new ArrayList<>();
                    n1.add(edge.getSecondNode());
                    mstNodesMap.put(edge.getFirstNode(), n1);
                }

                if (mstNodesMap.containsKey(edge.getSecondNode())) {
                    List<Node> n2 = mstNodesMap.get(edge.getSecondNode());
                    n2.add(edge.getFirstNode());
                    mstNodesMap.put(edge.getSecondNode(), n2);
                } else {
                    List<Node> n2 = new ArrayList<>();
                    n2.add(edge.getFirstNode());
                    mstNodesMap.put(edge.getSecondNode(), n2);
                }

                currentNumberOfClusters--;

            }
        }
    }

    private static boolean bfs(Node first, Node second) {

        Queue<Node> queue = new ArrayDeque<>();
        first.setExplored(true);
        queue.add(first);

        while (!queue.isEmpty()) {

            Node initial = queue.poll();
            List<Node> nodeList = mstNodesMap.getOrDefault(initial, null);

            if (nodeList != null) {
                for (Node n : nodeList) {
                    if (n.getValue() == second.getValue()) {
                        return true;
                    } else {
                        if (!n.isExplored()) {
                            n.setExplored(true);
                            queue.add(n);
                        }
                    }
                }
            }
        }

        return false;
    }

    private static void resetNodesToUnexplored() {
        for (Map.Entry<Node, List<Node>> entry : mstNodesMap.entrySet()) {
            entry.getKey().setExplored(false);
            entry.getValue().forEach(n -> n.setExplored(false));
        }

        edgesWithCostOne.forEach(n -> {
            n.getFirstNode().setExplored(false);
            n.getSecondNode().setExplored(false);
        });
        edgesWithCostTwo.forEach(n -> {
            n.getFirstNode().setExplored(false);
            n.getSecondNode().setExplored(false);
        });
    }


    private static void readFile(String fileName) {
        ClassLoader classLoader = new HammingClusteringAlg().getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try {
            Scanner sc = new Scanner(file);

            currentNumberOfClusters = sc.nextInt();

            BITS = sc.nextInt();

            int nodeIndex = 1;
            sc.nextLine();
            while (sc.hasNextLine()) {
                String vertex = sc.nextLine().replaceAll("\\s", "");

                if (verticesWithCodeAndIndex.containsKey(vertex)) {
                    List<Integer> vertexNr = verticesWithCodeAndIndex.get(vertex);
                    vertexNr.add(nodeIndex);
                    verticesWithCodeAndIndex.put(vertex, vertexNr);
                } else {
                    List<Integer> vertexNr = new ArrayList<>();
                    vertexNr.add(nodeIndex);
                    verticesWithCodeAndIndex.put(vertex, vertexNr);

                }

                zeroHammingDistances.put(vertex, Collections.singletonList(vertex));

                List<String> oneHammingList = new ArrayList<>();

                for (int i = 0; i < BITS; i++) {
                    oneHammingList.add(vertex.substring(0, i) + (vertex.charAt(i) ^ XOR_CONSTANT) + vertex.substring(i + 1, vertex.length()));
                }
                oneHammingDistances.put(vertex, oneHammingList);


                List<String> twoHammingList = new ArrayList<>();
                for (int i = 0; i < BITS - 1; i++) {
                    for (int j = i + 1; j < BITS; j++) {
                        twoHammingList.add(vertex.substring(0, i) +
                                (vertex.charAt(i) ^ XOR_CONSTANT) +
                                vertex.substring(i + 1, j) +
                                (vertex.charAt(j) ^ XOR_CONSTANT) +
                                vertex.substring(j + 1, vertex.length()));

                    }
                }
                twoHammingDistances.put(vertex, twoHammingList);
                nodeIndex++;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
