package coursera.algorithms_stanford_university.course_three.b_week_two;

public class Edge {

    private Node firstNode;

    private Node secondNode;

    private int cost;

    public Edge(Node firstNode, Node secondNode, int cost) {
        this.firstNode = firstNode;
        this.secondNode = secondNode;
        this.cost = cost;
    }

    public Node getFirstNode() {
        return firstNode;
    }

    public void setFirstNode(Node firstNode) {
        this.firstNode = firstNode;
    }

    public Node getSecondNode() {
        return secondNode;
    }

    public void setSecondNode(Node secondNode) {
        this.secondNode = secondNode;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Edge edge = (Edge) o;

        return cost == edge.getCost() && secondNode.getValue() ==edge.secondNode.getValue() && firstNode.getValue() == edge.firstNode.getValue();
    }

    @Override
    public int hashCode() {
        int result = firstNode.getValue();
        result = 31 * result + secondNode.getValue();
        result = 31 * result + cost;
        return result;
    }

    @Override
    public String toString() {
        return "Edge{" +
                "firstNode=" + firstNode +
                ", secondNode=" + secondNode +
                ", cost=" + cost +
                '}';
    }
}
