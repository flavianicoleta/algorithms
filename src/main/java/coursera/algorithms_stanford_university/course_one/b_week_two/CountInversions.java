package coursera.algorithms_stanford_university.course_one.b_week_two;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CountInversions {
    private static long count = 0;

    public static void main(String[] args) throws FileNotFoundException {

        ClassLoader classLoader = new CountInversions().getClass().getClassLoader();
        File file = new File(classLoader.getResource("inversions.txt").getFile());

        Scanner sc = new Scanner(file);

        int[] n = new int[100000];

        int i = 0;
        while (sc.hasNextLine()) {
            n[i++] = Integer.parseInt(sc.nextLine());
        }

//        int[] a = {1, 3, 5, 2, 4, 6 };
        countAll(n, n.length);

        System.out.println(count);
    }

    private static void countAll(int[] a, int n) {
        if (n < 2)
            return;
        int mid = n / 2;
        int[] l = new int[mid];
        int[] r = new int[n - mid];

        for (int i = 0; i < mid; i++) {
            l[i] = a[i];
        }
        for (int i = mid; i < n; i++) {
            r[i - mid] = a[i];
        }
        countAll(l, mid);
        countAll(r, n - mid);

        count(a, l, r, mid, n-mid);
    }


    private static void count(int[] a, int[] l, int[] r, int left, int right) {

        int i = 0, j = 0, k = 0;

        while (i < left && j < right) {

            if (l[i] < r[j])
                a[k++] = l[i++];
            else {
                a[k++] = r[j++];
                count += left - i;
            }

        }

        while (i < left)
            a[k++] = l[i++];

        while (j < right)
            a[k++] = r[j++];
    }
}
