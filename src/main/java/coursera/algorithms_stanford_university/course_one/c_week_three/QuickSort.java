package coursera.algorithms_stanford_university.course_one.c_week_three;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class QuickSort {

    private static long total = 0;

    public static void main(String[] args) throws FileNotFoundException {
        ClassLoader classLoader = new QuickSort().getClass().getClassLoader();
        File file = new File(classLoader.getResource("quickSort.txt").getFile());

        Scanner sc = new Scanner(file);

        int[] arr = new int[10000];
        int i = 0;
        while (sc.hasNext()) {
            arr[i++] = Integer.parseInt(sc.nextLine());
        }
//        System.out.println(Arrays.toString(arr));
        quickSort(arr, 0, arr.length);
//        System.out.println(Arrays.toString(arr));

//        int arr1[] = new int[]{3, 8, 6, 5, 1, 4, 11, 2, 10, 14, 13, 9, 7, 15, 12};
//        System.out.println(Arrays.toString(arr1));
//        quickSort(arr1, 0, arr1.length);
//        System.out.println(Arrays.toString(arr1));

        System.out.println(total);

    }

    private static void quickSort(int[] arr, int left, int right) {
        if ((right-left) < 1) {
            return;
        }

        int pivotPosition = partition(arr, left, right);
        quickSort(arr, left, pivotPosition);
        quickSort(arr, pivotPosition + 1, right);

    }

    private static int partition(int[] arr, int left, int right) {
        total += right - left - 1;
        int aux;

        // Case 1:162085
        // nothing else

        // Case 2: 164123
//        aux = arr[left];
//        arr[left] = arr[right - 1];
//        arr[right - 1] =  aux;

        //Case 3: 138382
        int len = right - left;
        int mid = len % 2 == 0 ? len / 2 - 1 + left : len / 2 + left;
        int first = arr[left];
        int last = arr[right - 1];
        int median = arr[mid];

        if ((median < first && median > last) || (median < last && median > first)) {
            arr[left] = median;
            arr[mid] =  first;
        } else if ((last < median && last > first) || (last < first && last > median)) {
            arr[left] = last;
            arr[right - 1] =  first;
        }
        //end case 3

        int p = arr[left];

        int i = left + 1;
        for (int j = left + 1; j < right; j++) {
            if (arr[j] < p) {
                aux = arr[i];
                arr[i] = arr[j];
                arr[j] = aux;
                i++;
            }
        }
        arr[left] = arr[i - 1];
        arr[i - 1] = p;

        return i - 1;
    }

}
