package coursera.algorithms_stanford_university.course_one.d_week_four;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

public class MinimumCut {

    public static void main(String[] args) throws FileNotFoundException {

        int min = 100;
        for (int i=1; i<=100; i++) {
            System.out.print(i + ": ");
            int nr = test();
            if (nr < min) {
                min = nr;
            }
        }

        System.out.println("-------------------------------");
        System.out.println("MIN: " + min);
    }

    private static int test() {
        Graph graph = readFile();

//        Graph graph = dummyGraph();


        if (graph != null) {

            while (graph.getVertices().size() > 2) {
                int randomEdgeIndex = random(graph.getEdges().size());

                graph.contractEdge(randomEdgeIndex);
            }

            System.out.println(graph.getEdges().size());

            return graph.getEdges().size();
        }

        return -1;
    }

    private static Graph dummyGraph() {
        List<Vertex> vertices = new LinkedList<>();
        List<Edge> edges = new LinkedList<>();

        vertices.add(new Vertex(1, Arrays.asList(2, 3).stream().collect(Collectors.toSet())));
        vertices.add(new Vertex(2, Arrays.asList(1, 3, 4).stream().collect(Collectors.toSet())));
        vertices.add(new Vertex(3, Arrays.asList(1, 2, 4).stream().collect(Collectors.toSet())));
        vertices.add(new Vertex(4, Arrays.asList(2, 3).stream().collect(Collectors.toSet())));

        edges.add(new Edge(1, 1, 2));
        edges.add(new Edge(2, 1, 3));
        edges.add(new Edge(3, 3, 4));
        edges.add(new Edge(4,2, 4));
        edges.add(new Edge(5, 2, 3));

        return new Graph(vertices, edges);
    }

    private static Graph readFile() {

        ClassLoader classLoader = new RandomizedSelection().getClass().getClassLoader();
        File file = new File(classLoader.getResource("kargerMinCut.txt").getFile());

        try {
            Scanner sc = new Scanner(file);

            List<Vertex> vertices = new LinkedList<>();
            List<Edge> edges = new LinkedList<>();

            int index = 1;

            while (sc.hasNextLine()) {

                Scanner sc2 = new Scanner(sc.nextLine());

                Vertex vertex = new Vertex();
                int firstVertex = sc2.nextInt();

                Set<Integer> linkedVertices = new HashSet<>();

                while (sc2.hasNextInt()){
                    int secondVertex = sc2.nextInt();
                    linkedVertices.add(secondVertex);
                    Edge edge = new Edge(index++, firstVertex, secondVertex);

                    if (!edges.contains(edge)) {
                        edges.add(edge);
                    }

                }

                vertex.setValue(firstVertex);
                vertex.setLinkedVertices(linkedVertices);
                vertices.add(vertex);

            }
            return new Graph(vertices, edges);

        } catch (FileNotFoundException e) {
            return null;
        }

    }

    private static int random(int max) {
        Random random = new Random();
        return random.nextInt(max);
    }
}
