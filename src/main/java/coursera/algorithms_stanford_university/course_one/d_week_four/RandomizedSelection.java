package coursera.algorithms_stanford_university.course_one.d_week_four;

public class RandomizedSelection {


    public static void main(String[] args) {
        int arr[] = new int[]{3, 8, 6, 5, 1, 4, 11, 2, 10, 14, 13, 9, 7, 15, 12};
//        System.out.println(Arrays.toString(arr));

        int ithElem = rSelect(arr, 0, arr.length, 9);

        System.out.println(ithElem);
    }

    private static int rSelect(int[] arr, int left, int right, int nr) {
//        if (arr.length == 1) {
//            return arr[0];
//        }

        int pivotPosition = partition(arr, left, right);
        if (pivotPosition - left + 1 == nr) {
            return arr[pivotPosition];
        } else if (pivotPosition + 1 > nr) {
            return rSelect(arr, left, pivotPosition, nr);
        } else {
            return rSelect(arr, pivotPosition + 1, right, nr - pivotPosition - 1);
        }
    }

    private static int partition(int[] arr, int left, int right) {
        int p = arr[left];

        int i = left + 1;
        for (int j = left + 1; j < right; j++) {
            if (arr[j] < p) {
                int aux = arr[i];
                arr[i] = arr[j];
                arr[j] = aux;
                i++;
            }
        }

        arr[left] = arr[i - 1];
        arr[i - 1] = p;

        return p - 1;
    }

}
