package coursera.algorithms_stanford_university.course_one.d_week_four;

public class Edge {

    private int id;

    private int firstVertex;

    private int secondVertex;

    Edge(int id, int firstVertex, int secondVertex) {
        this.id = id;
        this.firstVertex = firstVertex;
        this.secondVertex = secondVertex;
    }

    int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    int getFirstVertex() {
        return firstVertex;
    }

    void setFirstVertex(int firstVertex) {
        this.firstVertex = firstVertex;
    }

    int getSecondVertex() {
        return secondVertex;
    }

    void setSecondVertex(int secondVertex) {
        this.secondVertex = secondVertex;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Edge edge = (Edge) o;

        if (firstVertex != edge.firstVertex && firstVertex != edge.secondVertex) return false;
        return secondVertex == edge.secondVertex || secondVertex == edge.firstVertex;
    }

    @Override
    public int hashCode() {
        return 31 * (firstVertex + secondVertex);
    }

    @Override
    public String toString() {
        return "Edge{" +
                "FV=" + firstVertex +
                ", SV=" + secondVertex +
                '}';
    }
}
