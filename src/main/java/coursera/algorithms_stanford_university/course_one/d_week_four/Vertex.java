package coursera.algorithms_stanford_university.course_one.d_week_four;

import java.util.Set;

public class Vertex {

    private int value;
    private Set<Integer> linkedVertices;

    Vertex() {
    }

    Vertex(int value, Set<Integer> linkedVertices) {
        this.value = value;
        this.linkedVertices = linkedVertices;
    }

    int getValue() {
        return value;
    }

    void setValue(int value) {
        this.value = value;
    }

    Set<Integer> getLinkedVertices() {
        return linkedVertices;
    }

    void setLinkedVertices(Set<Integer> linkedVertices) {
        this.linkedVertices = linkedVertices;
    }

    @Override
    public String toString() {
        return "Vertex{" +
                "v=" + value +
                ", LV=" + linkedVertices +
                '}';
    }
}
