package coursera.algorithms_stanford_university.course_one.d_week_four;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Graph {

    private static int initialSize;

    private List<Vertex> vertices = new LinkedList<>();

    private List<Edge> edges = new LinkedList<>();

    Graph(List<Vertex> vertices, List<Edge> edges) {
        this.vertices = vertices;
        this.edges = edges;
        initialSize = vertices.size();
    }

    List<Vertex> getVertices() {
        return vertices;
    }

    List<Edge> getEdges() {
        return edges;
    }

    void contractEdge(int index) {

        Edge selectedEdge = edges.get(index);

        Vertex firstVertex = getVertex(selectedEdge.getFirstVertex());
        Vertex secondVertex = getVertex(selectedEdge.getSecondVertex());

        Set<Integer> linkedVertices = new HashSet<>();
        linkedVertices.addAll(firstVertex.getLinkedVertices());
        linkedVertices.addAll(secondVertex.getLinkedVertices());
        linkedVertices = linkedVertices.stream().filter(v -> v != firstVertex.getValue() && v != secondVertex.getValue()).collect(Collectors.toSet());

        edges.removeIf(e -> e.getId() == selectedEdge.getId());

        vertices.removeIf(v -> v.getValue() == firstVertex.getValue() || v.getValue() == secondVertex.getValue());

        int newVertexValue = addNewVertex(linkedVertices);

        for (Vertex vertex : vertices) {
            vertex.getLinkedVertices().removeIf(v -> v == firstVertex.getValue() || v == secondVertex.getValue());
            if (vertex.getValue() != newVertexValue) {
                vertex.getLinkedVertices().add(newVertexValue);
            }
        }

        for (Edge edge : edges) {
            if (edge.getFirstVertex() == firstVertex.getValue() || edge.getFirstVertex() == secondVertex.getValue()) {
                edge.setFirstVertex(newVertexValue);
            }

            if (edge.getSecondVertex() == firstVertex.getValue() || edge.getSecondVertex() == secondVertex.getValue()) {
                edge.setSecondVertex(newVertexValue);
            }
        }

        edges.removeIf(e -> e.getFirstVertex() == e.getSecondVertex());

    }

    private Vertex getVertex(int value) {
        return vertices.stream().filter(v -> v.getValue() == value).findFirst().orElse(null);
    }

    private int addNewVertex(Set<Integer> linkedVertices) {
        int newValue = ++initialSize;
        Vertex newVertex = new Vertex();
        newVertex.setValue(newValue);
        newVertex.setLinkedVertices(linkedVertices);
        vertices.add(newVertex);

        return newValue;
    }

    @Override
    public String toString() {
        return "Graph{" +
                "vertices=" + vertices +
                ", edges=" + edges +
                '}';
    }
}
