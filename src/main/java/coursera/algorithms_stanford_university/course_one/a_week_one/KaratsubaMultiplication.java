package coursera.algorithms_stanford_university.course_one.a_week_one;

import java.math.BigInteger;

public class KaratsubaMultiplication {

    private static String TEN = "10";

    public static void main(String[] args) {
        BigInteger x = new BigInteger("3141592653589793238462643383279502884197169399375105820974944592");
        BigInteger y = new BigInteger("2718281828459045235360287471352662497757247093699959574966967627");

        BigInteger a = new BigInteger("1234");
        BigInteger b = new BigInteger("3421");

        System.out.println(compute(a, b));
        System.out.println(compute(x, y));
    }

    private static BigInteger compute(BigInteger x, BigInteger y) {
        if (x.intValue() < 10 && y.intValue() < 10) {
            return x.multiply(y);
        } else {

            int n = Math.max(String.valueOf(x.longValue()).length(), String.valueOf(y.longValue()).length());
            int m = n / 2;

            BigInteger tenAtPowerM = new BigInteger(TEN).pow(m);
            BigInteger tenAtPowerDoubleM = new BigInteger(TEN).pow(m*2);

            BigInteger x_H = x.divide(tenAtPowerM);
            BigInteger x_L = x.mod(tenAtPowerM);

            BigInteger y_H = y.divide(tenAtPowerM);
            BigInteger y_L = y.mod(tenAtPowerM);

            BigInteger a = compute(x_H, y_H);
            BigInteger d = compute(x_L, y_L);
            BigInteger e = compute(x_H.add(x_L), y_H.add(y_L)).subtract(a).subtract(d);

            return (a.multiply(tenAtPowerDoubleM)).add(e.multiply(tenAtPowerM)).add(d);

        }
    }
}
