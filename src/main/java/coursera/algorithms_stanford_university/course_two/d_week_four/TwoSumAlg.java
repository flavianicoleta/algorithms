package coursera.algorithms_stanford_university.course_two.d_week_four;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class TwoSumAlg {

    private static Map<Long, Long> valueMap = new HashMap<>();

    private static final int MIN = -10000;
    private static final int MAX = 10000;

    private static int count = 0;

    public static void main(String[] args) {

        readFile("alg2sum.txt");

        compute();

        System.out.println(count);
    }

    private static void compute() {

        for (int target = MIN; target<= MAX; target++) {
            compute(target);
        }

    }

    private static void compute(int target) {
        for (Long value : valueMap.keySet()) {
            long secondValue = target - value;

            if (secondValue != value) {
                Long searchKey = valueMap.get(secondValue);
                if (searchKey != null) {
                    count++;
                    break;
                }

            }
        }
    }

    private static void readFile(String fileName) {
        ClassLoader classLoader = new TwoSumAlg().getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try {
            Scanner sc = new Scanner(file);

            while (sc.hasNextLong()) {
                long value =  sc.nextLong();

                valueMap.put(value, 1L);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
