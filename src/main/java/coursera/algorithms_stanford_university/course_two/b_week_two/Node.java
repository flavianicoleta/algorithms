package coursera.algorithms_stanford_university.course_two.b_week_two;

public class Node {

    private int vertex;

    private int value;

    public Node(int vertex, int value) {
        this.vertex = vertex;
        this.value = value;
    }

    public int getVertex() {
        return vertex;
    }

    public void setVertex(int vertex) {
        this.vertex = vertex;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Node{" +
                "vertex=" + vertex +
                ", value=" + value +
                '}';
    }
}
