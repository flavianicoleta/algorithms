package coursera.algorithms_stanford_university.course_two.b_week_two;

import java.io.File;
import java.util.*;

public class DijkstraAlg {

    private static final Integer START_VERTEX = 1;

//    private static final int SIZE = 5;
    private static final int SIZE = 201;

    private static ArrayList<Node>[] nodes = new ArrayList[SIZE];

    private static Set<Integer> processed = new HashSet<>();

    private static int[] shortestPaths = new int[SIZE];

    public static void main(String[] args) {

        readFile("DijkstraData.txt");

        dijkstra();

        System.out.println(Arrays.toString(shortestPaths));

        //7,37,59,82,99,115,133,165,188,197
        //2599,2610,2947,2052,2367,2399,2029,2442,2505,3068

        System.out.println("--------------------------------");

        System.out.print(shortestPaths[7] + ",");
        System.out.print(shortestPaths[37] + ",");
        System.out.print(shortestPaths[59] + ",");
        System.out.print(shortestPaths[82] + ",");
        System.out.print(shortestPaths[99] + ",");
        System.out.print(shortestPaths[115] + ",");
        System.out.print(shortestPaths[133] + ",");
        System.out.print(shortestPaths[165] + ",");
        System.out.print(shortestPaths[188] + ",");
        System.out.print(shortestPaths[197]);
    }

    private static void dijkstra() {
        processed.add(START_VERTEX);

        shortestPaths[START_VERTEX] = 0;

        while (processed.size() != SIZE - 1) {

            int dijkstraGreedyCriterion = Integer.MAX_VALUE;
            int newVertexToAdd = 0;

            for (int currentVertex : processed) {

                for (Node node : nodes[currentVertex]) {

                    if (!processed.contains(node.getVertex())) {
                        int localMin = shortestPaths[currentVertex] + node.getValue();

                        if (localMin < dijkstraGreedyCriterion) {
                            dijkstraGreedyCriterion = localMin;
                            newVertexToAdd = node.getVertex();
                        }
                    }

                }
            }

            if (newVertexToAdd != 0) {
                processed.add(newVertexToAdd);
                shortestPaths[newVertexToAdd] = dijkstraGreedyCriterion;
            }

        }

    }

    private static void readFile(String fileName) {
        ClassLoader classLoader = new DijkstraAlg().getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        Node node;

        try {
            Scanner sc = new Scanner(file);

            while (sc.hasNextLine()) {
                Scanner sc2 = new Scanner(sc.nextLine());

                int vertex = sc2.nextInt();

                nodes[vertex] = new ArrayList<>();

                while (sc2.hasNext()) {
                    String currentVertex = sc2.next();

                    String[] nodeAndValue = currentVertex.split(",");

                    node = new Node(Integer.parseInt(nodeAndValue[0]), Integer.parseInt(nodeAndValue[1]));

                    nodes[vertex].add(node);
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
