package coursera.algorithms_stanford_university.course_two.c_week_three;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Scanner;

public class MedianMaintenanceAlg {

    public static void main(String[] args)  {

//        computeMedian("MedianTest.txt");
        computeMedian("Median.txt"); //sum: 46831213; sum % 10000 = 1213

    }

    private static void computeMedian(String fileName) {
        ClassLoader classLoader = new MedianMaintenanceAlg().getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        PriorityQueue<Integer> minHeap = new PriorityQueue<>(new MaxComparator());
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>(new MinComparator());

        try {
            Scanner sc = new Scanner(file);

            int minHeapSize;
            int maxHeapSize;

            long sum = 0;

            while (sc.hasNextInt()) {
                int currentNr = sc.nextInt();

                if (minHeap.size() > 0 && currentNr < minHeap.peek()) {
                    minHeap.offer(currentNr);

                    minHeapSize = minHeap.size();
                    maxHeapSize = maxHeap.size();

                    if (minHeapSize > maxHeapSize + 1) {
                        maxHeap.offer(minHeap.poll());
                    }
                } else if (maxHeap.size() > 0 && currentNr > maxHeap.peek()) {
                    maxHeap.offer(currentNr);

                    minHeapSize = minHeap.size();
                    maxHeapSize = maxHeap.size();

                    if (maxHeapSize > minHeapSize + 1) {
                        minHeap.offer(maxHeap.poll());
                    }
                } else {
                    minHeapSize = minHeap.size();
                    maxHeapSize = maxHeap.size();

                    if (minHeapSize <= maxHeapSize) {
                        minHeap.offer(currentNr);
                    } else {
                        maxHeap.offer(currentNr);
                    }
                }

                minHeapSize = minHeap.size();
                maxHeapSize = maxHeap.size();

                if (minHeapSize >= maxHeapSize) {
                    sum += minHeap.peek();
                } else {
                    sum += maxHeap.peek();
                }

            }

            System.out.println(sum);

            long result = Math.floorMod(sum, 10000);

            System.out.println(result);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }

    private static class MinComparator implements Comparator<Integer> {

        @Override
        public int compare(Integer o1, Integer o2) {
            if (o1 < o2) {
                return -1;
            } else if (o1 > o2) {
                return 1;
            }
            return 0;
        }
    }

    private static class MaxComparator implements Comparator<Integer> {

        @Override
        public int compare(Integer o1, Integer o2) {
            if (o1 > o2) {
                return -1;
            } else if (o1 < o2) {
                return 1;
            }
            return 0;
        }
    }
}
