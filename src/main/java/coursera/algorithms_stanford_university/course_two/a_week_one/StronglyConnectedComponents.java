package coursera.algorithms_stanford_university.course_two.a_week_one;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;


public class StronglyConnectedComponents {

    private static long t = 0;
    private static Map<String, Long> counts = new HashMap<>();

    private static Map<Long, Vertex> vertices = new HashMap<>();

    private static Map<Long, Vertex> newVertices = new HashMap<>();

    public static void main(String[] args) {

        Graph graph = readFile("SCC_TEST.txt");

        if (graph != null) {

            dfsLoop(graph);

        }

    }

    public static void dfsLoop(Graph graph) {
//        Collections.sort(graph.getVertices(), Comparator.comparing(Vertex::getValue).reversed());

        vertices = graph.getVertices().stream().collect(Collectors.toMap(Vertex::getValue, v -> v));

        List<Long> keys = new ArrayList<>(vertices.keySet());

        Collections.sort(keys, Comparator.comparing(Long::longValue).reversed());

        Iterator iterator = keys.iterator();
        while (iterator.hasNext()) {
            Vertex currentVertex = vertices.get(iterator.next());
            if (!currentVertex.isExplored()) {
                dfs1(currentVertex);
            }
        }

        for (Vertex v : vertices.values()) {
            v.setExplored(false);
        }

        newVertices = graph.getVertices().stream().collect(Collectors.toMap(Vertex::getNewValue, v -> v));

        keys = new ArrayList<>(newVertices.keySet());

        Collections.sort(keys, Comparator.comparing(Long::longValue).reversed());

        iterator = keys.iterator();

        while (iterator.hasNext()) {
            Vertex currentVertex = newVertices.get(iterator.next());
            if (!currentVertex.isExplored()) {
                dfs2(currentVertex, 0L, String.valueOf(currentVertex.getNewValue()));
            }
        }
        System.out.println(counts);


    }

    public static void dfs1(Vertex vertex) {
        vertex.setExplored(true);

        if (vertex.getInVertices() != null) {
            for (Long i : vertex.getInVertices()) {
                Vertex secondVertex = vertices.get(i);
                if (!secondVertex.isExplored()) {
                    dfs1(secondVertex);
                }
            }
            t++;
            vertex.setNewValue(t);
        }

    }

    public static void dfs2(Vertex vertex, Long count, String leader) {
        vertex.setExplored(true);
        vertices.get(vertex.getValue()).setExplored(true);
        newVertices.get(vertex.getNewValue()).setExplored(true);

        if (vertex.getOutVertices() != null) {
            for (Long i : vertex.getOutVertices()) {
                count++;
                Vertex secondVertex = vertices.get(i);
                if (!secondVertex.isExplored()) {
                    dfs2(secondVertex, count, leader);
                } else {
                    if (!counts.containsKey(leader)) {
                        counts.put(leader, count);
                    }
                }
            }
        }
    }



    public static Graph readFile(String fileName) {
        ClassLoader classLoader = new StronglyConnectedComponents().getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try {
            Scanner sc = new Scanner(file);

//            List<Vertex> currentVerti = new ArrayList<>();

            Map<Long, Set<Long>> outVertices = new HashMap<>();
            Map<Long, Set<Long>> inVertices = new HashMap<>();

            while (sc.hasNextLine()) {

                Scanner sc2 = new Scanner(sc.nextLine());

                long firstVertex = sc2.nextLong();
                long secondVertex = sc2.nextLong();

                if (outVertices.containsKey(firstVertex)) {
                    outVertices.get(firstVertex).add(secondVertex);
                } else {
                    Set<Long> outSet = new HashSet<>();
                    outSet.add(secondVertex);
                    outVertices.put(firstVertex, outSet);
                }

                if (inVertices.containsKey(secondVertex)) {
                    inVertices.get(secondVertex).add(firstVertex);
                } else {
                    Set<Long> inSet = new HashSet<>();
                    inSet.add(firstVertex);
                    inVertices.put(secondVertex, inSet);
                }

//                if (!vertices.contains(new Vertex(firstVertex))) {
//                    vertices.add(new Vertex(firstVertex));
//                }
//
//
//                if (!vertices.contains(new Vertex(secondVertex))) {
//                    vertices.add(new Vertex(secondVertex));
//                }

                vertices.put(firstVertex, new Vertex(firstVertex));
                vertices.put(secondVertex, new Vertex(secondVertex));

            }

            for (Vertex vertex : vertices.values()) {
                vertex.setOutVertices(outVertices.get(vertex.getValue()));
                vertex.setInVertices(inVertices.get(vertex.getValue()));
            }

            return new Graph(new ArrayList<>(vertices.values()));

        } catch (FileNotFoundException e) {
            return null;
        }

    }
}
