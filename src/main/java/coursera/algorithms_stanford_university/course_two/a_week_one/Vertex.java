package coursera.algorithms_stanford_university.course_two.a_week_one;

import java.util.Set;

public class Vertex {

    private Long value;

    private Set<Long> outVertices;

    private Set<Long> inVertices;

    private boolean explored;

    private long newValue;

    public Vertex() {
    }

    public Vertex(Long value) {
        this.value = value;
        this.explored = false;
        this.newValue = 0;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public Set<Long> getOutVertices() {
        return outVertices;
    }

    public void setOutVertices(Set<Long> outVertices) {
        this.outVertices = outVertices;
    }

    public Set<Long> getInVertices() {
        return inVertices;
    }

    public void setInVertices(Set<Long> inVertices) {
        this.inVertices = inVertices;
    }

    public boolean isExplored() {
        return explored;
    }

    public void setExplored(boolean explored) {
        this.explored = explored;
    }

    public Long getNewValue() {
        return newValue;
    }

    public void setNewValue(Long newValue) {
        this.newValue = newValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vertex vertex = (Vertex) o;

        return value == vertex.value;
    }

    @Override
    public int hashCode() {
        return (int) (value ^ (value >>> 32));
    }

    @Override
    public String toString() {
        return "Vertex{" +
                "value=" + value +
                ", explored=" + explored +
                ", newValue=" + newValue +
                "} \n";
    }

//    @Override
//    public int compareTo(Object o) {
//        Vertex vertex = (Vertex) o;
//
//        if (this.value < vertex.value) return -1;
//        else if (this.value > vertex.value) return 1;
//        else return 0;
//    }
}

