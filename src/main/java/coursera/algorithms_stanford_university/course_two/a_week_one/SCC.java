package coursera.algorithms_stanford_university.course_two.a_week_one;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

/*
    434821,968,459,313,211
 */

public class SCC {

//    private static final int SIZE = 10;
    private static final int SIZE = 875715;

    private static int TIME = 0;

    private static List<Integer>[] inVertices = new ArrayList[SIZE];
    private static List<Integer>[] outVertices = new ArrayList[SIZE];
    private static boolean[] explored = new boolean[SIZE];
    private static int[] newOrder = new int[SIZE];
    private static int[] leaders = new int[SIZE];
    private static int source;

    public static void main(String[] args) {

        readFile("SCC.txt");

        dfsLoops();

//        Arrays.sort(newOrder);
        Arrays.sort(leaders);
//        System.out.println(Arrays.toString(leaders));

        for (int i = 1; i<=5; i++) {
            System.out.print(leaders[SIZE - i] + ",");
        }
    }
    //600497,318,202,181,177
    //600516,318,297,274,246
    private static void dfsLoops() {

        for (int node = SIZE - 1; node > 0; node--) {
            if (!explored[node]) {
                dfs11(node);
            }
        }

        explored = new boolean[SIZE];

        for (int node = SIZE - 1; node > 0; node--) {

            int currentVertex = newOrder[node];
            if (!explored[currentVertex]) {
                source = currentVertex;
                dfs22(currentVertex);
            }
        }

    }

    private static void dfs1(int node) {

        Iterator<Integer>[] adj = (Iterator<Integer>[]) new Iterator[SIZE];
        for (int v = 1; v < SIZE; v++) {
            if (inVertices[v] == null) {
                inVertices[v] = new ArrayList<>();
            }
            adj[v] = inVertices[v].iterator();
        }

        Stack<Integer> stack = new Stack<>();
        explored[node] = true;
        stack.push(node);

        while (!stack.empty()) {
            int vertex = stack.peek();
            if (adj[vertex].hasNext()) {
                int secondVertex = adj[vertex].next();

                if (!explored[secondVertex]) {
                    explored[secondVertex] = true;

                    stack.push(secondVertex);
                } else {
                    if (newOrder[vertex] == 0) {
                        TIME++;
                        newOrder[vertex] = TIME;
                    }

                }
            } else {
                stack.pop();

                if (newOrder[vertex] == 0) {
                    TIME++;
                    newOrder[vertex] = TIME;
                }
            }
        }
    }

    private static void dfs2(int node) {

        Iterator<Integer>[] adj = (Iterator<Integer>[]) new Iterator[SIZE];
        for (int v = 1; v < SIZE; v++) {
            if (outVertices[v] == null) {
                outVertices[v] = new ArrayList<>();
            }
            adj[v] = outVertices[v].iterator();
        }

        Stack<Integer> stack = new Stack<>();
        explored[node] = true;
        stack.push(node);

        while (!stack.empty()) {
            int vertex = stack.peek();

            if (adj[vertex].hasNext()) {


                int secondVertex = adj[vertex].next();

                if (!explored[secondVertex]) {

                    explored[secondVertex] = true;

                    stack.push(secondVertex);
                } else {

                }
            } else {
                leaders[source]++;
                stack.pop();

            }
        }
    }

    private static void dfs11(int node) {
        explored[node] = true;
        for (int vertex : inVertices[node]) {
            if (!explored[vertex]) {
                dfs11(vertex);
            }
        }
        TIME++;
        newOrder[TIME] = node;


    }

    private static void dfs22(int node) {
        explored[node] = true;
        leaders[source]++;
        for (int vertex : outVertices[node]) {
            if (!explored[vertex]) {
                dfs22(vertex);
            }
        }
    }

    private static void readFile(String fileName) {

        ClassLoader classLoader = new StronglyConnectedComponents().getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try {
            Scanner sc = new Scanner(file);

            while (sc.hasNextInt()) {
                int firstNode = sc.nextInt();
                int secondNode = sc.nextInt();

                if (outVertices[firstNode] == null) {
                    outVertices[firstNode] = new ArrayList<>();
                }

                if (inVertices[secondNode] == null) {
                    inVertices[secondNode] = new ArrayList<>();
                }

                if (inVertices[firstNode] == null) {
                    inVertices[firstNode] = new ArrayList<>();
                }

                if (outVertices[secondNode] == null) {
                    outVertices[secondNode] = new ArrayList<>();
                }

                outVertices[firstNode].add(secondNode);
                inVertices[secondNode].add(firstNode);
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
